#!/usr/bin/env
from importlib import import_module
import string
from typing import Union, Type, Any
import rospy
from std_msgs import msg
from std_msgs.msg import String
import paho.mqtt.client as mqtt
import ssl
import json
import sys
import random
import time
from logging.handlers import RotatingFileHandler
import logging
from pathlib import Path


class MESSAGE_BRIDGE:
    def __init__(self) -> None:
        params = rospy.get_param("uts_ros_project", {})

        aws_mqtt_server_params = params.pop("aws_mqtt_server", {})
        self.mqtt_root_topic = aws_mqtt_server_params.pop("mqtt_root_topic", {})
        connection_params = aws_mqtt_server_params.pop("connection")
        self.mqtt_broker_address = connection_params.pop("host")
        self.mqtt_broker_port = connection_params.pop("port")
        self.mqtt_broker_keep_alive = connection_params.pop("keepalive")

        tls_params = aws_mqtt_server_params.pop("tls_params")
        self.ca_certs_location = tls_params.pop("ca_certs_location")
        tls_version = tls_params.pop("tls_version")
        if tls_version == "tls_v1.0":
            self.tls_version =ssl.PROTOCOL_TLSv1
        elif tls_version == "tls_v1.1":
            self.tls_version =ssl.PROTOCOL_TLSv1_1
        elif tls_version == "tls_v1.2":
            self.tls_version =ssl.PROTOCOL_TLSv1_2
        else:
            self.tls_version =ssl.PROTOCOL_TLSv1_2
        self.tls_insecure = tls_params.pop("tls_insecure")


        field_device_params = params.pop("field_device_params")
        self.site_name = field_device_params.pop("site_name")
        self.client_id = field_device_params.pop("device_id")
        self.device_name = field_device_params.pop("device_name")
        self.anonymous_node = field_device_params.pop("anonymous_node")

        self.device_sensor_upload_topic = field_device_params.pop("device_senser_upload_topic")
        self.device_sensor_download_topic = field_device_params.pop("device_senser_download_topic")
        
        ros_mqtt_agent_params = params.pop("ros_mqtt_agent_params", {})
        self.this_app_ros_node_name = ros_mqtt_agent_params.pop("ros_node_name", {})
        self.agents_ros_to_mqtt = ros_mqtt_agent_params.get("agents_ros_to_mqtt", [])
        self.agents_mqtt_to_ros =  ros_mqtt_agent_params.get("agents_mqtt_to_ros", [])

        
        self.mqtt_root_topic_sensor_gets = "uts_iot_prototype_new" + self.site_name + self.device_name + "/" + self.client_id + "/sensor_gets"
        self.mqtt_root_topic_sensor_sets = "uts_iot_prototype_new" + self.site_name + self.device_name + "/" + self.client_id + "/sensor_sets"

        #print(MESSAGE_BRIDGE.get_full_topic_str(self.mqtt_root_topic, self.site_name, self.device_name, self.client_id, self.device_sensor_upload_topic))
        #print(MESSAGE_BRIDGE.get_full_topic_str(self.mqtt_root_topic, self.site_name, self.device_name, self.client_id, self.device_sensor_download_topic))

        self.mqtt_send_command_topic = "/led_control"

    @staticmethod
    def get_full_topic_str(*argv: string) -> string:
        full_topic_str = ""

        for arg in argv:
            if full_topic_str == "":
                full_topic_str = full_topic_str + arg
            else:
                full_topic_str = full_topic_str + "/" + arg
        return full_topic_str
    
    @staticmethod
    def lookup_object(object_path: str, package: str='ROS_MQTT_AGENT') -> Any:
        """ lookup object from a some.module:object_name specification. """
        module_name, obj_name = object_path.split(":")
        module = import_module(module_name, package)
        obj = getattr(module, obj_name)
        return obj

    def mqtt_cloud_server_connect(self):
        self.mqtt_client = mqtt.Client(self.client_id)
        self.mqtt_client.on_connect = self.callback_mqtt_on_connect
        self.mqtt_client.on_disconnect = self.callback_mqtt_on_disconnect
        self.mqtt_client.on_message = self.callback_mqtt_on_msg
        self.mqtt_client.on_log = self.callback_mqtt_on_log
        
        self.mqtt_client.tls_set(self.ca_certs_location, certfile = None, keyfile = None, cert_reqs = ssl.CERT_REQUIRED, tls_version = self.tls_version)
        self.mqtt_client.tls_insecure_set(self.tls_insecure)
        self.__mqtt_connect()
        
    def callback_mqtt_on_msg(self,client, userdata, msg):
        l.debug("Message received, callback collector fired.")

    def callback_mqtt_on_connect(self,client, userdata, flags, rc):
        self.loop_flag = 0
        if rc == 0:
            l.debug("Connected to mqtt broker.")
            self.mqtt_is_connected = True
        else:
            print("connectiong error: return code = ", rc)
            l.debug(f"connectiong error: return code = {rc}")


    def callback_mqtt_on_log(self,client, userdata, level, buf):
        pass
        #print("[MQTT_LOG]: " + buf)

    def callback_mqtt_on_disconnect(self,client, obj, rc):
        if rc == 0:
            l.debug("Disconneted peacefully from mqtt broker.")
        else:
            l.debug("Disconneted from mqtt broker unexpectly, maybe network error.")
            self.mqtt_is_connected = False


    def __mqtt_connect(self):
        l.debug(f'CONNECTING TO BROKER {self.mqtt_broker_address}')
        self.mqtt_client.connect(self.mqtt_broker_address, self.mqtt_broker_port, self.mqtt_broker_keep_alive)
        #+++++++++++++++++++++++++++++++++++++ waiting for on_connect callback
        self.loop_flag=1
        self.mqtt_client.loop_start()
        cnt = 0
        while self.loop_flag ==1:
            l.debug(f"waiting for on_connect() callback counter: {cnt}")
            time.sleep(0.01)
            cnt += 1
        #+++++++++++++++++++++++++++++++++++++

    def start_bridge(self):
        agent_pool=[]
        rospy.init_node(self.this_app_ros_node_name, anonymous = self.anonymous_node)
        device_sensor_download_topic = MESSAGE_BRIDGE.get_full_topic_str(self.mqtt_root_topic, self.site_name, self.device_name, self.client_id, self.device_sensor_download_topic)
        self.mqtt_client.subscribe(device_sensor_download_topic +"/#" ,0)

        for agent in self.agents_ros_to_mqtt:
            ros_topic = agent.pop("ros_topic")
            mqtt_topic = MESSAGE_BRIDGE.get_full_topic_str(self.mqtt_root_topic, self.site_name, self.device_name, self.client_id, self.device_sensor_upload_topic, agent.pop("mqtt_topic"))
            agent_pool.append(ROS_TO_MQTT_AGENT(ros_topic, mqtt_topic, \
                    MESSAGE_BRIDGE.lookup_object(agent.pop("msg_type")), self.mqtt_client))
            l.debug(f"Add agent task [ros to mqtt]: {ros_topic} -> {mqtt_topic}")

        for agent in self.agents_mqtt_to_ros:
            mqtt_topic = MESSAGE_BRIDGE.get_full_topic_str(self.mqtt_root_topic, self.site_name, self.device_name, self.client_id, self.device_sensor_download_topic, agent.pop("mqtt_topic"))
            ros_topic = "/" + agent.pop("ros_topic")
            agent_pool.append(MQTT_TO_ROS_AGENT(mqtt_topic, ros_topic, \
                    MESSAGE_BRIDGE.lookup_object(agent.pop("msg_type")), self.mqtt_client))
            l.debug(f"Add agent task [mqtt to ros]: {mqtt_topic} -> {ros_topic}")
        
        rospy.spin()
        

    
class ROS_TO_MQTT_AGENT:
    def __init__(self, ros_topic, mqtt_topic, ros_msg_type: Union[str, Type[rospy.Message]], mqtt_client, queue_size = 10) -> None:
        self.ros_topic = ros_topic
        self.mqtt_topic = mqtt_topic
        self.ros_msg_type = ros_msg_type
        self.mqtt_client = mqtt_client
        self.queue_size = queue_size
        self.___assign_work()
        
    def callbackfunc(self, data):
        l.debug(f'Message forwarded from ros topic: {self.ros_topic} to mqtt topic: {self.mqtt_topic} [data value]: {data.data}')
        self.mqtt_client.publish(self.mqtt_topic,data.data,1,True)

    def ___assign_work(self):    
        sub_from_ros = rospy.Subscriber(self.ros_topic,self.ros_msg_type,self.callbackfunc,queue_size= self.queue_size)


class MQTT_TO_ROS_AGENT:
    def __init__(self, mqtt_topic, ros_topic, ros_msg_type: Union[str, Type[rospy.Message]], mqtt_client, queue_size = 10) -> None:
        self.ros_topic = ros_topic
        self.mqtt_topic = mqtt_topic
        self.ros_msg_type = ros_msg_type
        self.mqtt_client = mqtt_client
        self.queue_size = queue_size        #rospy.loginfo("mqtt led control sub topic: " + self.mqtt_topic)
        self.___assign_work()

    #callback function whenever a message is received.
    def callbackfunc(self, client, userdata, msg):
        l.debug(f'Message received from mqtt topic: {self.mqtt_topic} to ros topic: {self.ros_topic} [data value]: {msg.payload.decode("utf-8")}')
        if msg.topic == self.mqtt_topic:
            pub = rospy.Publisher(self.ros_topic, self.ros_msg_type, queue_size=10)
            pub.publish(msg.payload.decode("utf-8"))

    def ___assign_work(self):    
        self.mqtt_client.message_callback_add(self.mqtt_topic, self.callbackfunc)
        l.debug("mqtt led control sub topic: " + self.mqtt_topic)        



if __name__ == '__main__':
    l = logging.getLogger("ros_mqtt_agent_logger")
    l.setLevel(logging.DEBUG)

    console_print_h = logging.StreamHandler()
    console_print_h.setLevel(logging.DEBUG)
    formatter = logging.Formatter("%(levelname)s - %(asctime)s - %(message)s")
    console_print_h.setFormatter(formatter)

    script_dir = Path(__file__)
    log_filename = "log.txt"
    rotating_file_h = RotatingFileHandler(filename = str(script_dir.parent.absolute()) + '/' + log_filename, mode = 'a', maxBytes = 20000000, backupCount = 10, encoding = "utf-8", delay = False)
    rotating_file_h.setFormatter(formatter)

    l.addHandler(console_print_h)
    l.addHandler(rotating_file_h)

    try:
        msg_bridge = MESSAGE_BRIDGE()
        msg_bridge.mqtt_cloud_server_connect()
        msg_bridge.start_bridge()

    except rospy.ROSInterruptException:
        pass

